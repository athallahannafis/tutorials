extends KinematicBody2D

export (int) var speed = 400
export (int) var jumpSpeed = 500
export (int) var GRAVITY = 1200
var dashSpeed = 200
var JUMPS = 1
var jumpLimit = 2
var canDash = true

const UP = Vector2(0,-1)

var velocity = Vector2()

func get_input():
	if is_on_floor(): JUMPS = 1
	velocity.x = 0
	if Input.is_action_pressed('right'):
		velocity.x += speed
	if Input.is_action_pressed('left'):
		velocity.x -= speed
	if Input.is_action_just_pressed("up") and is_on_floor():
		jump()
	if Input.is_action_just_pressed("up") and !is_on_floor():
		velocity.y = 0
		if JUMPS < jumpLimit:
			JUMPS += 1
			jump()
	if Input.is_action_pressed("left") and Input.is_key_pressed(KEY_A):
		velocity.x -= speed + dashSpeed
	if Input.is_action_pressed("right") and Input.is_key_pressed(KEY_A):
		velocity.x += speed + dashSpeed

func jump():
	velocity.y -= jumpSpeed

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)
